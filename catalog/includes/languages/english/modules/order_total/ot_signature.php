<?php
/**
 * Order Total Module
 *
 *
 * @package - Signature Required
 * @copyright Copyright 2007 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_signature.php 3 2011-09-16 00:09:23Z numinix $
 */

  define('MODULE_ORDER_TOTAL_SIGNATURE_TITLE', 'Signature Required');
  define('MODULE_ORDER_TOTAL_SIGNATURE_DESCRIPTION', 'Signature Required');
  define('MODULE_ORDER_TOTAL_SIGNATURE_TEXT_ENTER_CODE', '<p>Is a signature required?</p>');
//eof