<?php
/**
 * Order Total Module
 *
 *
 * @package - Signature Required
 * @copyright Copyright 2007-2008 Numinix Technology http://www.numinix.com
 * @copyright Copyright 2003-2007 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: ot_signature.php 3 2011-09-16 00:09:23Z numinix $
 */

class ot_signature {

  var $title, $output, $enabled;
  
  function ot_signature()
  {
	  global $order, $currencies, $db;
	  
    $this->code = 'ot_signature';
    $this->title = MODULE_ORDER_TOTAL_SIGNATURE_TITLE;
    $this->description = MODULE_ORDER_TOTAL_SIGNATURE_DESCRIPTION;
    $this->enabled = ((MODULE_ORDER_TOTAL_SIGNATURE_STATUS == 'true') ? true : false);
    $this->sort_order = MODULE_ORDER_TOTAL_SIGNATURE_SORT_ORDER;
    $this->output = array();
    $this->credit_class = 'true';
    $this->applicable_methods = array();

    $shipping_methods = explode(',', MODULE_ORDER_TOTAL_SIGNATURE_SHIPPING_METHODS);
    foreach($shipping_methods as $method) {
    	if(!empty(trim($method))){
    		$this->applicable_methods[] = trim(strtolower($method));
    	}
    }

    $geozones = $db->Execute("SELECT * FROM " . TABLE_GEO_ZONES);
    
    if ($this->enabled == true) {
      $this->check_for_enabled();

			if (MODULE_ORDER_TOTAL_SIGNATURE_ZONE > 0) {
        $this->enabled = false;
				$check = $db->Execute("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . MODULE_ORDER_TOTAL_SIGNATURE_ZONE . "' and zone_country_id = '" . $order->delivery['country']['id'] . "' order by zone_id");
				while (!$check->EOF) {
					if ( ($check->fields['zone_id'] < 1) || ($check->fields['zone_id'] == $order->delivery['zone_id']) ) {
            $this->enabled = true;
						break;
					}
					$check->MoveNext();
				} // end while
			}
		}
  }
 
  function check_for_enabled() {
    $this->enabled = false;
    $chosen_method = isset($_SESSION['shipping']['id']) ? $_SESSION['shipping']['id'] : $_SESSION['shipping'];
    //if the configuration parameter is blank, apply to all
    if(empty($this->applicable_methods)){
    	$this->enabled = true;
    }else{
    	foreach($this->applicable_methods as $method) {
    		if(strstr($chosen_method, $method)) {
    			$this->enabled = true;
    		}
    	}
    }
  } 
  
  function process() {
    global $order, $currencies, $db;

    if ($this->enabled) {
	    //$order_total = $this->get_order_total();
	    $order_total_signature = $_SESSION['cart']->show_total();	    
	    if (MODULE_ORDER_TOTAL_SIGNATURE_FREE_SHIPPING == 'true') {
		    $order_total_signature = $order_total_signature - $_SESSION['cart']->free_shipping_prices();
	    }

      $cart_content_type = $_SESSION['cart']->get_content_type();
      $gv_content_only = $_SESSION['cart']->gv_only();
      if ($cart_content_type == 'physical' or $cart_content_type == 'mixed') {
        $charge_it = true;
      } else {
        // check to see if everything is virtual, if so - skip the low order fee.
        if ((($cart_content_type == 'virtual') and MODULE_ORDER_TOTAL_SIGNATURE_VIRTUAL == 'true')) {
          $charge_it = false;
          if ((($gv_content_only > 0) and MODULE_ORDER_TOTAL_SIGNATURE_GV == 'false')) {
            $charge_it = true;
          }
        }
          
        if ((($gv_content_only > 0) and MODULE_ORDER_TOTAL_SIGNATURE_GV == 'true')) {
          // check to see if everything is gift voucher, if so - skip the low order fee.
          $charge_it = false;
          if ((($cart_content_type == 'virtual') and MODULE_ORDER_TOTAL_SIGNATURE_VIRTUAL == 'false')) {
            $charge_it = true;
          }
        }
      }
      //end else

        
      if (!isset($_SESSION['signature']) || !$_SESSION['signature']) {
        $charge_it = 'false';
      }
      if (isset($_SESSION['opt_signature']) && $_SESSION['opt_signature']) {
          $charge_it = 'true';
      }
      if ($order_total_signature > MODULE_ORDER_TOTAL_SIGNATURE_REQUIRED) {
        $charge_it = 'true';
      }
        
      if ($charge_it == 'true') {
        $signature_fee = MODULE_ORDER_TOTAL_SIGNATURE_FEE;
        if (!$shipping_id) {
          $shipping_id = $_SESSION['shipping']['id'];
          $module = substr($shipping_id, 0, strpos($shipping_id, '_'));
        }
        if ($GLOBALS[$module]->tax_class > 0) {
          if (!defined($GLOBALS[$module]->tax_basis)) {
            $shipping_tax_basis = STORE_SHIPPING_TAX_BASIS;
          } else {
            $shipping_tax_basis = $GLOBALS[$module]->tax_basis;
          }
            
          if ($shipping_tax_basis == 'Billing') {
            $tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
            $tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
          } elseif ($shipping_tax_basis == 'Shipping') {
            $tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
            $tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
          } else {
            if (STORE_ZONE == $order->billing['zone_id']) {
              $tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
              $tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->billing['country']['id'], $order->billing['zone_id']);
            } elseif (STORE_ZONE == $order->delivery['zone_id']) {
              $tax = zen_get_tax_rate($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
              $tax_description = zen_get_tax_description($GLOBALS[$module]->tax_class, $order->delivery['country']['id'], $order->delivery['zone_id']);
            } else {
              $tax = 0;
            }
          }
        }
        $order->info['tax_groups']["$tax_description"] += zen_calculate_tax($signature_fee, $tax);
        $order->info['tax'] += zen_calculate_tax($signature_fee, $tax);
        $order->info['total'] += $signature_fee + zen_calculate_tax($signature_fee, $tax);
        $this->output[] = array('title' => $this->title . ':',
        'text' => $currencies->format($signature_fee, true, $order->info['currency'], $order->info['currency_value']),
        'value' => $signature_fee);
      } 
		}
	}

	function pre_confirmation_check($order_total) {
  }
  
  function credit_selection() {
	  global $order, $db, $currencies;
	  if ($this->enabled) {
		  //$order_total = $this->get_order_total(); // the actual order total
			$order_total_signature = $_SESSION['cart']->show_total(); // used to calculate the total that will be used for the signature
			if (MODULE_ORDER_TOTAL_SIGNATURE_FREE_SHIPPING == 'true') {
		    $order_total_signature = $order_total_signature - $_SESSION['cart']->free_shipping_prices();
	    }
			
      $signature_fee = MODULE_ORDER_TOTAL_SIGNATURE_FEE;
	     
	    $selected = (($_SESSION['opt_signature'] == '1') ? true : false);
	    $display_signature = true;
	    if ( ($order_total_signature >= MODULE_ORDER_TOTAL_SIGNATURE_REQUIRED) ) {
		    $display_signature = false;
	    }
	    if ($display_signature) { // signature is not required
        if (in_array($_GET['main_page'], array(FILENAME_CHECKOUT, FILENAME_QUICK_CHECKOUT))) { // support FEC and FEAC			    
          $selection = array('id' => $this->code,
			    'module' => $this->title,
			    'redeem_instructions' => MODULE_ORDER_TOTAL_SIGNATURE_TEXT_ENTER_CODE,
			    'fields' => array(array('field' => zen_draw_checkbox_field('opt_signature', '1', $selected, 'id="opt_signature" onclick="updateForm();"'),
			        										'title' => $currencies->format($signature_fee, true, $order->info['currency'], $order->info['currency_value'])
			    )));
		    } else {
			    $selection = array('id' => $this->code,
			    'module' => $this->title,
			    'redeem_instructions' => MODULE_ORDER_TOTAL_SIGNATURE_TEXT_ENTER_CODE,
			    'fields' => array(array('field' => zen_draw_checkbox_field('opt_signature', '1', $selected, 'id="opt_signature"'),
			        										'title' => $currencies->format($signature_fee, true, $order->info['currency'], $order->info['currency_value'])										
			    )));
		    }
	    } else { // signature is required, selection not needed 
		    $selection = false;
	    }
	    return $selection;
    }
  }
  
  function get_order_total() {
    global $order;
    $order_total_tax = $order->info['tax'];
    $order_total = $order->info['total'];
    if ($this->include_shipping != 'true') $order_total -= $order->info['shipping_cost'];
    if ($this->include_tax != 'true') $order_total -= $order->info['tax'];
    $orderTotalFull = $order_total;
    $order_total = array('totalFull'=>$orderTotalFull, 'total'=>$order_total, 'tax'=>$order_total_tax);

    return $order_total;
  }

  function update_credit_account($i)
  {
  }

  function apply_credit()
  {
  }
  
  function clear_posts()
  {
    unset($_SESSION['signature']);
  }
  
  function collect_posts()
  {
    global $db, $currencies;
    if ($_POST['opt_signature']) {
        $_SESSION['signature'] = $_POST['opt_signature'];
    } else {
        $_SESSION['signature'] = '0';
    }
  }
  
  function check()
  {
    global $db;
    if (!isset($this->check)) {
      $check_query = $db->Execute("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = 'MODULE_ORDER_TOTAL_SIGNATURE_STATUS'");
      $this->check = $check_query->RecordCount();
    }
    
    return $this->check;
  }
  
  function keys()
  {
    $keys = array('MODULE_ORDER_TOTAL_SIGNATURE_STATUS', 'MODULE_ORDER_TOTAL_SIGNATURE_SORT_ORDER', 'MODULE_ORDER_TOTAL_SIGNATURE_FEE', 'MODULE_ORDER_TOTAL_SIGNATURE_TAX_CLASS', 'MODULE_ORDER_TOTAL_SIGNATURE_VIRTUAL', 'MODULE_ORDER_TOTAL_SIGNATURE_GV', 'MODULE_ORDER_TOTAL_SIGNATURE_FREE_SHIPPING', 'MODULE_ORDER_TOTAL_SIGNATURE_REQUIRED', 'MODULE_ORDER_TOTAL_SIGNATURE_SHIPPING_METHODS', 'MODULE_ORDER_TOTAL_SIGNATURE_ZONE');
		return $keys;
  }
  
  function install()
  {
    global $db;
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Signature Required Module', 'MODULE_ORDER_TOTAL_SIGNATURE_STATUS', 'true', 'Do you want to enable this module? To fully turn this off, both this option and the one below should be set to false.', '6', '1','zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values('Sort Order', 'MODULE_ORDER_TOTAL_SIGNATURE_SORT_ORDER', '299', 'Sort order of display.', '6', '3', now())");
		$db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, date_added) values('Signature Required Cost', 'MODULE_ORDER_TOTAL_SIGNATURE_FEE', '4.00', 'What amount should be charged for the signature required option?', '6', '7', 'currencies->format', now())");
	  $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values('Tax Class', 'MODULE_ORDER_TOTAL_SIGNATURE_TAX_CLASS', '0', 'Use the following tax class on the signature fee.', '6', '10', 'zen_get_tax_class_title', 'zen_cfg_pull_down_tax_classes(', now())");
	  $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values('No Signature Fee on Virtual Products', 'MODULE_ORDER_TOTAL_SIGNATURE_VIRTUAL', 'true', 'Do not charge signature fee when cart is Virtual Products Only', '6', '11', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
	  $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values('No Signature Fee on Gift Vouchers', 'MODULE_ORDER_TOTAL_SIGNATURE_GV', 'true', 'Do not charge signature fee when cart is Gift Vouchers only', '6', '12', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
	  $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values('No Signature Fee on Free Shipping', 'MODULE_ORDER_TOTAL_SIGNATURE_FREE_SHIPPING', 'true', 'Do not calculate signature fee for products that have free shipping (includes gv and virtual products)', '6', '13', 'zen_cfg_select_option(array(\'true\', \'false\'), ', now())");
	  $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, date_added) values('Required Signature Amount', 'MODULE_ORDER_TOTAL_SIGNATURE_REQUIRED', '100', 'Automatically charge signature fee for amounts over X dollars', 6, '14', 'currencies->format', now())");
                 $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added)
values('Applicable Shipping Methods', 'MODULE_ORDER_TOTAL_SIGNATURE_SHIPPING_METHODS', '', 'The charge will only be applied when the customer chooses one of the shipping methods specified here.  Leave blank to apply to all methods.  Use the shipping module codes separated by commas, e.g. \'usps,ontracwebservices\'', '6', '7', now())");
    $db->Execute("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, use_function, set_function, date_added) values ('Zone', 'MODULE_ORDER_TOTAL_SIGNATURE_ZONE', '0', 'If a zone is selected, only enable this module for that zone', '6', '0', 'zen_get_zone_class_title', 'zen_cfg_pull_down_zone_classes(', now())");     
  }
  
	function remove() {
		global $db;
		$db->Execute("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
	}
}
?>
